source 'https://rubygems.org'
ruby '2.3.1'

group :development do
  gem 'activesupport' # replaces non-ascii characters with an ascii approximation <http://api.rubyonrails.org/classes/ActiveSupport/Inflector.html#method-i-transliterate>
  gem 'awesome_print' # nice highlighting/indentation in ruby console <https://github.com/michaeldv/awesome_print>
  gem 'backup' # backup system utility
  gem 'brakeman' # static security vulnerability scanner for rails <http://brakemanscanner.org>, <https://github.com/presidentbeef/brakeman>
  gem 'csvlint' # validate csv a-la: `csvlint path/to/file.csv` # path/to/file.csv is VALID
  gem 'flay' # find code structural similarities `flay path/to/source` <https://github.com/seattlerb/flay>
  gem 'heroku'
  gem 'html2slim'
  gem 'htty' # http statefull interactive client (a-la ftp client)
  gem 'mailcatcher' # `pkill --full mailcatcher ; mailcatcher --http-ip 0.0.0.0` <https://github.com/sj26/mailcatcher>
  gem 'mdl' # markdown lint tool <https://github.com/mivok/markdownlint>
  gem 'rails'
  gem 'rails_best_practices' # code metric tool for rails `rails_best_practices path/to/source `
  gem 'reek' # code smell detector for ruby `reek path/to/source` <https://github.com/troessner/reek>
  gem 'roodi' # find ruby code design issues `roodi path/to/source` <https://github.com/roodi/roodi>
  gem 'rubocop' # code analyzer based on ruby style guide used by emacs flycheck `rubocop path/to/source` <https://github.com/bbatsov/rubocop>
  gem 'ruby-lint' # used by emacs flycheck
  gem 'rubycritic' # generate ruby code quality report <https://github.com/whitesmith/rubycritic>
  gem 'sandi_meter' # analysis ruby code `sandi_meter path/to/source` <https://github.com/makaroni4/sandi_meter>
  gem 'scss_lint' # scss checker used by emacs flycheck <https://github.com/brigade/scss-lint>
  gem 'scss_lint_reporter_checkstyle' # used by emacs flycheck
  gem 'sqlint' # sql linter <https://github.com/purcell/sqlint>
  gem 't' # create twitter timeline backup <http://blog.jphpsf.com/2012/05/07/backing-up-your-twitter-account-with-t>
  gem 'tailor' # check style of ruby files (indentation and so on) `tailor path/to/source` <https://github.com/turboladen/tailor>
  gem 'travis'
end
