# TODO

## Key bindings

Try https://github.com/baskerville/sxhkd

## Go

* https://github.com/apex/apex (aws lambda)
* https://github.com/eawsy (aws lambda)
* https://github.com/elwinar/rambler (sql migrations)
* https://github.com/gliderlabs/sigil
* https://github.com/haya14busa/reviewdog (post linter comments to github)
* https://github.com/jedisct1/piknik (copy/paste over the network)

## Rust

* https://github.com/faradayio/cage (deploy docker containers)
* https://github.com/jedisct1/edgedns (dns cache)
* https://github.com/mattgreen/watchexec (executes commands in response to file modifications)
* https://github.com/nivekuil/rip (a-la `rm` with backup/restore)
