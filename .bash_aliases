# This file is part of Danil Kutkevich <danil@kutkevich.org> home.

export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_TYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

# Tab completion
# <http://wiki.gentoo.org/wiki/Bash#Tab_completion>.
[[ -f /etc/profile.d/bash-completion.sh ]] && source /etc/profile.d/bash-completion.sh

# Disable the XOFF (Ctrl-s) keystroke
# <http://superuser.com/questions/124845/can-you-disable-the-ctrl-s-xoff-keystroke-in-putty#155875>.
stty -ixon

export EDITOR="vim" #export EDITOR="vim" #export EDITOR="/usr/bin/emacsclient -t"
# export ALTERNATE_EDITOR="/usr/bin/emacs"
export GIT_EDITOR='emacs'
# export PAGER="/usr/bin/less -IM" #not working(
export HISTSIZE=30000
export HISTFILESIZE=30000
export HISTCONTROL=ignoredups:erasedups
# See /usr/share/terminfo/*/
# export TERM=rxvt-256color
[ "$TERM" = "xterm" ] && TERM="xterm-256color" #<http://ricochen.wordpress.com/2011/07/23/mac-os-x-lion-terminal-color-remote-access-problem-fix>
#export GIT_PAGER=""

# Aliases.
# <http://askubuntu.com/questions/22037/aliases-not-available-when-using-sudo#22043>.
alias sudo='sudo '
alias ls='ls --color'
alias ll='ls -l --all --human-readable'
# alias less=$PAGER
alias e='emacs --no-window-system'
alias ec='emacsclient --tty'
alias ag='ag --width=5000'

export PATH="$HOME"/bin:"$PATH" #clib is an C package manager <https://github.com/clibs/clib> run `c-install-all`

export PATH="$PATH":"$HOME"/deps/bin #bpkg bash package manager <https://github.com/bpkg/bpkg#installing-packages> run `bash-install-all`

export PATH="$HOME/.cask/bin:$PATH" #emacs cask <http://cask.github.io>

# Go <http://golang.org/doc/code.html#GOPATH>.
# Run `go-install-all`.
export GOPATH="$HOME"/go
export PATH="$PATH":"$GOPATH"/bin #for convenience, add the workspace's bin subdirectory to your PATH
# [[ -s "$HOME/.gvm/scripts/gvm" ]] && source "$HOME/.gvm/scripts/gvm" #gvm (Go version manager) <https://github.com/moovweb/gvm>

# Rust (rust toolchain installer https://www.rustup.rs).
export PATH="$HOME"/.cargo/bin:"$PATH" #be sure to add `/home/danil/.cargo/bin` to your PATH to be able to run the installed binaries

# Steel Bank Common Lisp.
export SBCL_HOME=/usr/lib64/sbcl

# rbenv <https://github.com/sstephenson/rbenv#basic-github-checkout>.
export PATH="$HOME/.rbenv/bin:$PATH"
if hash rbenv 2>/dev/null; then
    eval "$(rbenv init -)"
fi

export PATH="$PATH":"$HOME"/.local/bin #pip (python package management system) run `python-install-all`

# # Lua.
# PATH="$HOME"/.luarocks/bin:"$PATH"
# export LUA_PATH="$HOME""/.luarocks/share/lua/5.1//?.lua;./?.lua;$LUA_PATH"
# export LUA_CPATH="$HOME""/.luarocks/lib/lua/5.1//?.so;./?.so;$LUA_CPATH"

# # lenv (Lua version manager) <https://github.com/mah0x211/lenv>.
# export PATH="$HOME"/.lenv/bin:"$HOME"/.lenv/current/bin:$PATH
# export LUA_PATH="$HOME"'/.lenv/current/luarocks/share/?.lua;'"$HOME"'/.lenv/current/luarocks/share/?/init.lua;;'
# export LUA_CPATH="$HOME"'/.lenv/current/luarocks/lib/?.so;;'

# Travis CI gem.
[ -f "$HOME"/.travis/travis.sh ] && source "$HOME"/.travis/travis.sh #auto completion

# Node.js and npm.
# WARNING: Do NOT give priority to npm executables!!!
export PATH="$PATH:$HOME/node_modules/.bin"

# n (Node.js version manager).
# Added by n-install (see http://git.io/n-install-repo).
export N_PREFIX="$HOME"/n
[[ :$PATH: == *":$N_PREFIX/bin:"* ]] || PATH+=":$N_PREFIX/bin"

# # nvm (Node.js version manager)
# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  #this loads nvm
# [[ -r "$NVM_DIR/bash_completion" ]] && . "$NVM_DIR/bash_completion"

# Prompt.
[[ -f "$HOME"/.bash_prompt.sh ]] && source "$HOME"/.bash_prompt.sh
