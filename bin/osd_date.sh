#! /bin/sh
# This file is part of Danil Kutkevich <danil@kutkevich.org> home.
# <http://en.wikipedia.org/wiki/Load_(computing)>
dunstify "$(LANG=ru_RU.UTF-8 date '+%R %a %e %h %Y %Z')"
