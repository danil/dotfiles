;;; This file is part of Danil Kutkevich <danil@kutkevich.org> home.

(in-package :stumpwm)

;;; Variables.
(defparameter MY-X-TERM "urxvt"
  "What shall be the command run when we want an X terminal?")
(defparameter MY-HOME-DIR (getenv "HOME"))
(defparameter MY-USER (getenv "USER"))
(defparameter MY-HOSTNAME (run-shell-command "hostname | tr -d [:cntrl:]" t))
(defparameter MY-PROMPT (concat MY-USER "@" MY-HOSTNAME))
(defparameter MY-FOREGROUND-COLOR "white")
(defparameter MY-BACKGROUND-COLOR "black")
(defparameter MY-INTERACTIVE-BORDER-COLOR "OrangeRed1")
(defparameter MY-MODELINE-FOREGROUND-COLOR "gray")
(defparameter MY-MODELINE-BACKGROUND-COLOR "black")
(defparameter MY-DATE-FORMAT "+%d-%b-%y %R")
(defparameter MY-DATE-COMMAND (format nil "date '~a' | tr -d [:cntrl:]"
                                      MY-DATE-FORMAT))
(defparameter MY-LOAD-COMMAND "~/bin/load.sh | tr -d [:cntrl:]")
(defparameter MY-BACKGROUND-IMAGE "~/local/share/backgrounds/gentoo-znurt-larry-abducted-1440x900.png")
(defparameter MY-CURSOR-NAME "Vanilla-DMZ")
(defparameter MY-SHOW-CPU-LOAD "bin/show_cpu_load.sh")
(defparameter MY-SHOW-BATTERY-STATUS "bin/show_battery_status.sh")
(defparameter MY-CONTRIB-DIR (merge-pathnames "src/vendor/cl/stumpwm-contrib/"
                               (user-homedir-pathname)))
(defparameter MY-PACKAGES '(
                            "util/stumptray/"
                            "modeline/cpu/"
                            "modeline/mem/"
                            ))

(set-prefix-key (kbd "M-m"))

;; Terminal emulator.
(define-key *root-map* (kbd "c") (concat "exec " MY-X-TERM))

;;; Commands
;;; <http://paste.lisp.org/display/75796>.
;; Select group and echo group nickname.
;; ;; Show CPU load.
;; (defcommand show-cpu-load () ()
;;   (message "~a" (run-shell-command
;;                  (concat MY-HOME-DIR "/"  MY-SHOW-CPU-LOAD) t)))
;; ;; Query ACPI and show the battery's status.
;; (defcommand show-battery-status () ()
;;   (message "~a" (run-shell-command
;;                  (concat MY-HOME-DIR "/" MY-SHOW-BATTERY-STATUS) t)))

;; Groups.
(setf (group-name (first (screen-groups (current-screen)))) "1") ;rename the first group "Default" to "1" <http://en.wikipedia.org/wiki/User:Gwern/.stumpwmrc>
;; Create a new groups but do not switch to it.
(gnewbg "2")
(gnewbg "3")
(gnewbg "4")
(gnewbg "5")
(gnewbg "6")
(gnewbg "7")
(gnewbg "8")
(gnewbg "9")

(defcommand my-dmenu-run () () (run-shell-command "~/bin/my_dmenu_run.sh"))
(defcommand my-set-keyboard-auto-repeat () ()
  (run-shell-command "xset r rate 300 30")) ;set the keyboard auto repeat parameters
(defcommand my-set-terminal-bell () ()
  (run-shell-command "xset b off")) ;turn off bell
(defcommand my-set-background (background) ()
  (run-shell-command (concatenate 'string "feh --bg-center " background)))
(defcommand my-set-cursor-name (cursor-name) ()
  (run-shell-command (concatenate 'string "xsetroot -cursor_name " cursor-name))) ;<https://github.com/stumpwm/stumpwm/wiki/FAQ#why-is-the-mouse-pointer-an-x-and-how-do-i-change-it>
(defcommand my-run-power-manager () () (run-shell-command "xfce4-power-manager"))
(defcommand my-run-wireless-manager () ()
  (run-shell-command
   (concatenate 'string
                "pkill -f /usr/share/wicd/gtk/wicd-client.py ; "
                "wicd-gtk --tray")))
(defcommand my-run-volume-daemon () ()
  (run-shell-command "killall xfce4-volumed ; xfce4-volumed"))
(defcommand my-run-dictionary () ()
  (run-shell-command "killall stardict ; stardict --hide"))
(defcommand my-run-unclutter () ()
  (run-shell-command "killall unclutter ; unclutter"))

;;; Key binding.
;; (define-key *root-map* (kbd "C-q") "show-battery-status") ;battery status
;; (define-key *root-map* (kbd "q") "show-cpu-load") ;cpu load
;; (define-key *top-map* (kbd "C-M-G") "vgroups") ;groups (virtual desktops or workspaces) <http://nongnu.org/stumpwm/manual/stumpwm_8.html>
(define-key *top-map* (kbd "C-M-M") "mode-line") ;mode line <http://nongnu.org/stumpwm/manual/stumpwm_7.html>
(define-key *root-map* (kbd "!") "my-dmenu-run")
;; (define-key *root-map* (kbd "x") "next")

;;; Mode line.
(setf *screen-mode-line-format*
      (list
       "[%n " MY-PROMPT "] "
       "%W "
       "^>" ;right align
       '(:eval (run-shell-command MY-LOAD-COMMAND t))
       " %t %M/%P% "
       '(:eval (run-shell-command MY-DATE-COMMAND t))
       "      "))

(setq *mode-line-position* :bottom)
(setf *mode-line-foreground-color* MY-MODELINE-FOREGROUND-COLOR)
(setf *mode-line-background-color* MY-MODELINE-BACKGROUND-COLOR)
(setf *mode-line-border-width* 0)

;; ;;; <https://github.com/stumpwm/stumpwm/issues/72>.
;; (load-module "ttf-fonts")
;; (xft:cache-fonts)
;; (set-font (make-instance 'xft:font :family "monospace" :subfamily "Regular" :size 12))

(set-msg-border-width 3)
(setf *input-window-gravity* :top-left)
(set-border-color MY-INTERACTIVE-BORDER-COLOR)
(set-fg-color "white")
(set-bg-color "black")

(dolist (MY-package MY-PACKAGES)
  (let ((MY-path (merge-pathnames MY-package MY-CONTRIB-DIR)))
    (when (probe-file MY-path)
      (push MY-path asdf:*central-registry*))))

;; (let ((quicklisp-init (merge-pathnames "src/vendor/cl/stumpwm-contrib/media/amixer/amixer.lisp"
;;                                        (user-homedir-pathname))))
;;   (when (probe-file quicklisp-init)
;;     (load quicklisp-init)))

(ql:quickload "stumptray") ;<https://github.com/stumpwm/stumpwm-contrib/blob/master/util/stumptray/README.org>.
;; (run-commands "stumptray")

(ql:quickload "cpu")
;; (load-module "cpu")

(ql:quickload "mem")
;; (load-module "mem")

(my-set-keyboard-auto-repeat)
(my-set-background MY-BACKGROUND-IMAGE)
(my-set-cursor-name MY-CURSOR-NAME)
(my-run-power-manager)
(my-run-wireless-manager)
(my-run-volume-daemon)
(my-run-dictionary)
(my-set-terminal-bell)
(my-run-unclutter)

(load "~/.stumpwm.d/init.lisp")
